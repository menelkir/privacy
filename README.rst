Privacy
=======

=============
What is this?
=============

The idea of this repository is some fixes and patches to fix the privacy on different operating systems and software (with the sources). All the rights are in each directory used. Feel free to pay a visit to the authors.

=============
How to donate
=============

If you find this repo useful (don't forget to pay a visit to the related
repos too), you can buy me a beer:

:BTC: 3ECzX5UhcFSRv6gBBYLNBc7zGP9UA5Ppmn

:ETH: 0x7E17Ac09Fa7e6F80284a75577B5c1cbaAD566C1c


