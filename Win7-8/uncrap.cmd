@echo off
openfiles.exe 1>nul 2>&1
if not %errorlevel% equ 0 (
    Echo You are not administrator! Right Click file select run as admin
    pause
    EXIT 1
) else (   

SETLOCAL

REM --- uninstall updates
echo Uninstalling updates...

echo Deleting KB971033 (license validation)
start "title" /b /wait wusa.exe /kb:971033 /uninstall /quiet /norestart

echo Deleting KB2902907 (Microsoft Security Essentials)
start "title" /b /wait wusa.exe /kb:2902907 /uninstall /quiet /norestart

echo Deleting KB2952664 (Get Windows 10 assistant)
start "title" /b /wait wusa.exe /kb:2952664 /uninstall /quiet /norestart

echo Deleting KB2976978 (update for windows 8.1 and windows 8)
start "title" /b /wait wusa.exe /kb:2976978 /uninstall /quiet /norestart

echo Deleting KB2977759 (update for windows 7 rtm)
start "title" /b /wait wusa.exe /kb:2977759 /uninstall /quiet /norestart

echo Deleting KB2990214 (Get Windows 10 for Win7)
start "title" /b /wait wusa.exe /kb:2990214 /uninstall /quiet /norestart

echo Deleting KB3012973 (Upgrade to Windows 10 Pro)
start "title" /b /wait wusa.exe /kb:3012973 /uninstall /quiet /norestart

echo Deleting KB3014460 (Upgrade for windows insider preview / upgrade to windows 10)
start "title" /b /wait wusa.exe /kb:3014460 /uninstall /quiet /norestart

echo Deleting KB3015249 (Upgrade that adds telemetry points to consent.exe in Windows 8.1 and Windows 7)
start "title" /b /wait wusa.exe /kb:3015249 /uninstall /quiet /norestart

echo Deleting KB3021917 (update to Windows 7 SP1 for performance improvements)
start "title" /b /wait wusa.exe /kb:3021917 /uninstall /quiet /norestart

echo Deleting KB3022345 (telemetry)
start "title" /b /wait wusa.exe /kb:3022345 /uninstall /quiet /norestart

echo Deleting KB3138612 (Windows Update Client for Windows 7)
start "title" /b /wait wusa.exe /kb:3138612 /uninstall /quiet /norestart

echo Deleting KB3081954 (Telemetry update for Windows)
start "title" /b /wait wusa.exe /kb:3081954 /uninstall /quiet /norestart

echo Deleting KB3035583 (GWX Update installs Get Windows 10 app in Windows 8.1 and Windows 7 SP1)
start "title" /b /wait wusa.exe /kb:3035583 /uninstall /quiet /norestart

echo Deleting KB3044374 (Get Windows 10 for Win8.1)
start "title" /b /wait wusa.exe /kb:3044374 /uninstall /quiet /norestart

echo Deleting KB3050265 (update for Windows Update on Win7)
start "title" /b /wait wusa.exe /kb:3050265 /uninstall /quiet /norestart

echo Deleting KB3138615 (Compactibility Update (another GWX))
start "title" /b /wait wusa.exe /kb:3138615 /uninstall /quiet /norestart

echo Deleting KB3050267 (update for windows update client for windows 8.1 june 2015)
start "title" /b /wait wusa.exe /kb:3050267 /uninstall /quiet /norestart

echo Deleting KB3065987 (update for Windows Update on Win7/Server 2008R2)
start "title" /b /wait wusa.exe /kb:3065987 /uninstall /quiet /norestart

echo Deleting KB3068708 (telemetry)
start "title" /b /wait wusa.exe /kb:3068708 /uninstall /quiet /norestart

echo Deleting KB3133977 (Buggy update)
start "title" /b /wait wusa.exe /kb:3133977 /uninstall /quiet /norestart

echo Deleting KB3075249 (telemetry for Win7/8.1)
start "title" /b /wait wusa.exe /kb:3075249 /uninstall /quiet /norestart

echo Deleting KB3075851 (update for Windows Update on Win7/Server 2008R2)
start "title" /b /wait wusa.exe /kb:3075851 /uninstall /quiet /norestart

echo Deleting KB3075853 (update for Windows Update on Win8.1/Server 2012R2)
start "title" /b /wait wusa.exe /kb:3075853 /uninstall /quiet /norestart

echo Deleting KB3080149 (Telemetry)
start "title" /b /wait wusa.exe /kb:3080149 /uninstall /quiet /norestart

echo Deleting KB3112336 (monitor quality of upgrade experience)
start "title" /b /wait wusa.exe /kb:3112336 /uninstall /quiet /norestart

echo Deleting KB2999226 (Update for Universal C Runtime in Windows)
start "title" /b /wait wusa.exe /kb:2999226 /uninstall /quiet /norestart

echo Deleting KB3118401 (Update for Universal C Runtime in Windows)
start "title" /b /wait wusa.exe /kb:3118401 /uninstall /quiet /norestart

echo Deleting KB3083710 (Windows Update Client for Windows 7 and Windows Server 2008 R2)
start "title" /b /wait wusa.exe /kb:3083710 /uninstall /quiet /norestart

echo Deleting KB3112343 (Windows Update Client for Windows 7 and Windows Server 2008 R2)
start "title" /b /wait wusa.exe /kb:3112343 /uninstall /quiet /norestart

echo Deleting KB3123862 (Updated capabilities to upgrade Windows 8.1 and Windows 7)
start "title" /b /wait wusa.exe /kb:3123862 /uninstall /quiet /norestart

echo Deleting KB3139929 (Security update for Internet Explorer)
start "title" /b /wait wusa.exe /kb:3139929 /uninstall /quiet /norestart

echo Deleting KB3146449 (Updated Internet Explorer 11 capabilities to upgrade Windows 8.1 and Windows 7)
start "title" /b /wait wusa.exe /kb:3146449 /uninstall /quiet /norestart

echo Deleting KB3150513 (Latest compatibility definition update for Windows)
start "title" /b /wait wusa.exe /kb:3150513 /uninstall /quiet /norestart

echo Deleting KB3125574 (Update for Windows 7 for x64-based Systems)
start "title" /b /wait wusa.exe /kb:3125574 /uninstall /quiet /norestart

echo Deleting KB2977759 (Compatibility update for Windows 7 RTM)
start "title" /b /wait wusa.exe /kb:2977759 /uninstall /quiet /norestart

echo Deleting KB3102810 (update for "Windows Update")
start "title" /b /wait wusa.exe /kb:3102810 /uninstall /quiet /norestart

echo Deleting KB2882822 (Update adds ITraceRelogger interface support)
start "title" /b /wait wusa.exe /kb:2882822 /uninstall /quiet /norestart

echo Deleting KB3184143 (Remove software related to the Windows 10 free upgrade offer)
start "title" /b /wait wusa.exe /kb:3184143 /uninstall /quiet /norestart

echo Deleting KB2976987 (description not available)
start "title" /b /wait wusa.exe /kb:2976987 /uninstall /quiet /norestart

echo Deleting KB3135445 (Windows 10 Upgrade for Windows 7)
start "title" /b /wait wusa.exe /kb:3135445 /uninstall /quiet /norestart

echo Deleting KB3139923 (Another GWX)
start "title" /b /wait wusa.exe /kb:3139923 /uninstall /quiet /norestart

echo Deleting KB3173040 (Another GWX)
start "title" /b /wait wusa.exe /kb:3173040 /uninstall /quiet /norestart

echo Deleting KB3172605 (July 2016 Rollup)
start "title" /b /wait wusa.exe /kb:3172605 /uninstall /quiet /norestart

echo Deleting KB3179573 (August 2016 Rollup)
start "title" /b /wait wusa.exe /kb:3179573 /uninstall /quiet /norestart

echo Deleting KB3185278 (September 2016 Rollup)
start "title" /b /wait wusa.exe /kb:3185278 /uninstall /quiet /norestart

echo Deleting KB3185330 (October 2016 Rollup)
start "title" /b /wait wusa.exe /kb:3185330 /uninstall /quiet /norestart

echo Deleting KB3192403 (October 2016 Rollup Preview)
start "title" /b /wait wusa.exe /kb:3192403 /uninstall /quiet /norestart

echo Deleting KB3197868 (November 2016 Rollup)
start "title" /b /wait wusa.exe /kb:3197868 /uninstall /quiet /norestart

echo Deleting KB3197869 (November 2016 Rollup Preview)
start "title" /b /wait wusa.exe /kb:3197869 /uninstall /quiet /norestart

echo Deleting KB3207752 (December 2016 Rollup)
start "title" /b /wait wusa.exe /kb:3207752 /uninstall /quiet /norestart

echo Deleting KB3212646 (January 2017 Rollup)
start "title" /b /wait wusa.exe /kb:3212646 /uninstall /quiet /norestart

echo Deleting KB4012215 (March 2017 Rollup)
start "title" /b /wait wusa.exe /kb:4012215 /uninstall /quiet /norestart

echo Deleting KB4012218 (March 2017 Rollup Preview)
start "title" /b /wait wusa.exe /kb:4012218 /uninstall /quiet /norestart

echo Deleting KB4015549 (April, 2017 Rollup)
start "title" /b /wait wusa.exe /kb:4015549 /uninstall /quiet /norestart

echo Deleting KB4015552 (April, 2017 Rollup Preview)
start "title" /b /wait wusa.exe /kb:4015552 /uninstall /quiet /norestart

echo Deleting KB4019264 (May, 2017 Rollup)
start "title" /b /wait wusa.exe /kb:4019264 /uninstall /quiet /norestart

echo Deleting KB4019265 (May, 2017 Rollup Preview)
start "title" /b /wait wusa.exe /kb:4019265 /uninstall /quiet /norestart

echo Deleting KB4022719 (July, 2017 Rollup)
start "title" /b /wait wusa.exe /kb:4022719 /uninstall /quiet /norestart

echo Deleting KB4022168 (July, 2017 Rollup Preview)
start "title" /b /wait wusa.exe /kb:4022168 /uninstall /quiet /norestart

echo Deleting KB4025341 (June, 2017 Rollup)
start "title" /b /wait wusa.exe /kb:4025341 /uninstall /quiet /norestart

echo Deleting KB4025340 (June, 2017 Rollup Preview)
start "title" /b /wait wusa.exe /kb:4025340 /uninstall /quiet /norestart

echo Deleting KB4034664 (August, 2017 Rollup)
start "title" /b /wait wusa.exe /kb:4034664 /uninstall /quiet /norestart

echo Deleting KB3188740 (November 2016 Rollup for .NET)
start "title" /b /wait wusa.exe /kb:3188740 /uninstall /quiet /norestart

echo Deleting KB3196686 (November 2016 .NET Rollup Preview)
start "title" /b /wait wusa.exe /kb:3196686 /uninstall /quiet /norestart

echo Deleting KB3205402 (December 2016 Rollup for .NET)
start "title" /b /wait wusa.exe /kb:3205402 /uninstall /quiet /norestart

echo Deleting KB4015549 (April 2017 Rollup for .NET)
start "title" /b /wait wusa.exe /kb:4019112 /uninstall /quiet /norestart

echo Deleting KB4015552 (April 2017 Rollup Preview for .NET)
start "title" /b /wait wusa.exe /kb:4019288 /uninstall /quiet /norestart

echo Deleting KB4019112 (May 2017 Rollup for .NET)
start "title" /b /wait wusa.exe /kb:4019112 /uninstall /quiet /norestart

echo Deleting KB4019288 (May 2017 Rollup Preview for .NET)
start "title" /b /wait wusa.exe /kb:4019288 /uninstall /quiet /norestart

echo - Done.
